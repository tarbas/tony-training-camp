// Let's create a fake service, like fetch, to emulate API call to a backend
const fakeService = () => {
    // Promise take an "executor" function which takes 2 callbacks (resolve, reject).
    // Resolve will run if executor do some successful work (get data from BE e.g.), and some value will pass to resolve CB,
    // and reject will run if executor faced with some error (can not get data from the BE e.g.).
    return new Promise((resolve, reject) => {
        // for emulating real BE like behaviour we use setTimeout to have some delay
        setTimeout(() => {
            //Here we calculate what should we fire: resolve or reject. Please check do you know what Math.random returns
            const isPromiseSucceeded = Math.random() > 0.5;
            console.log('%cisPromiseSucceeded:', 'color: #0e3f99; font-size: 18px', isPromiseSucceeded);

            isPromiseSucceeded ? resolve('Some data') : reject('Oops!');
        }, 2000);
    });
};

// We have 2 options how to deal with async code.
// First, it's using try catch, it's a preferable way due to clear code.
// So we just say: wait (await) until promise from the fakeService will be "fulfilled" (try block)
// or "rejected" (catch block).
// But you can use "await" notation only in async function, so that's why I created a foo function.
const foo = async () => {
    try {
        const response = await fakeService();

        alert(response);
    } catch (e) {
        alert(e);
    }
};

foo();

// The second options is using then() construction:
// fakeService().then(res => console.log(res)).catch(err => console.log(err));

// Please be aware, if you are just trying to receive data from promise without await or then(),
// you just received promise in the pending state
// try {
//     const response = fakeService();
//
//     console.log('%cresponse:', 'color: #0e3f99; font-size: 18px', response);
// } catch (error) {
//     console.log('%cerror:', 'color: #0e3f99; font-size: 18px', error);
// }

// Just uncomment different part of the code and try to understand how it works, good luck!
// https://learn.javascript.ru/promise-basics

